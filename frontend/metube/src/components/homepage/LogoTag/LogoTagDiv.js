import './LogoTagDiv.css';

function LogoTagDiv() {
    return (
        <div id="logoTagDiv">
            <a href={window.location.origin}>
                <img src={window.location.origin + '/logo.gif'} alt="MeTube logo"/>
            </a>
        </div>
    );
}

export default LogoTagDiv;
