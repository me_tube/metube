import './UtilDiv.css';

function UtilDiv() {
    return (
        <div id="utilDiv">
            <b><a href={window.location.origin}>Sign Up</a></b>
            <span className="utilDelim">|</span>
            <a href={window.location.origin}>My account</a>
            <span className="utilDelim">|</span>
            <a href={window.location.origin}>History</a>
            <span className="utilDelim">|</span>
            <a href={window.location.origin}>Help</a>
            <span className="utilDelim">|</span>
            <a href={window.location.origin}>Log In</a>
        </div>
    );
}

export default UtilDiv;
