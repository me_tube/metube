import './FeaturedCommunity.css';

function FeaturedCommunity(props) {
    return (
        <div id="featured_community">
            <div id="featured_community_pic">
                <a href={window.location.origin}>
                    <img id="featured_community_img" src={props.avatar} alt="community_pic" />
                </a>
            </div>
            <div id="featured_community_info">
                <b>
                    <a href={window.location.origin}>{props.name}</a>
                </b>
                <div id="featured_community_details">
                    {props.videos} Videos | {props.discussions} Discussions
                </div>
            </div>
        </div>
    );
}

export default FeaturedCommunity;
