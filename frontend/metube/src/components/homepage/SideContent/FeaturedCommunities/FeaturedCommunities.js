import './FeaturedCommunities.css';
import FeaturedCommunity from "./Community/FeaturedCommunity";

function FeaturedCommunities() {
    return (
        <div id="featured_communities">
            <div id="featured_communities_content">
                <div id="featured_communities_header">Featured Community</div>
                <FeaturedCommunity avatar={window.location.origin + '/icons/community_pic.jpg'} name="essownicy gimpera" videos="2137" discussions="420"/>
                <FeaturedCommunity avatar={window.location.origin + '/icons/community_pic.jpg'} name="essownicy gimpera" videos="2137" discussions="420"/>
                <div id="featured_communities_more">
                    <a href={window.location.origin}>See More Channels</a>
                </div>
            </div>
        </div>
    );
}

export default FeaturedCommunities;
