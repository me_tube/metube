import './PopularMobileVideos.css';
import PopularMobileVideo from './PopularMobileVideo/PopularMobileVideo'
function PopularMobileVideos() {
    return (
        <div id="popular_mobile">
            <div id="popular_mobile_content">
                <div id="popular_mobile_header">Popular Videos for Mobile Devices</div>
                <PopularMobileVideo avatar={window.location.origin + "/icons/video_pic.jpg"} name="orzel" author="orzel222" views="45"/>
                <PopularMobileVideo avatar={window.location.origin + "/icons/video_pic.jpg"} name="orzel" author="vjasieg" views="73"/>
                <div id="popular_mobile_more">
                    <a href={window.location.origin}>
                        See more mobile videos
                    </a>
                </div>
            </div>
        </div>
    );
}

export default PopularMobileVideos;
