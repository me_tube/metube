import './PopularMobileVideo.css';

function PopularMobileVideo(props) {
    return (
        <div id="popular_video">
            <div id="popular_video_pic">
                <a href={window.location.origin}>
                    <img id="popular_video_img" src={props.avatar} alt="popular_video_pic" />
                </a>
            </div>
            <div id="popular_video_info">
                <b>
                    <a href={window.location.origin}>
                        {props.name}
                    </a>
                </b>
                <div id="popular_video_details">
                    <span className="grayText">
                        From: <a href={window.location.origin}>{props.author}</a>
                    </span><br/>
                    <span className="grayText">Views: </span>{props.views}
                </div>
            </div>
        </div>
    );
}

export default PopularMobileVideo;
