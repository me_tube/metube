import './FeaturedChannel.css';

function FeaturedChannel(props) {
    return (
        <div id="featured_channel">
            <div id="featured_channel_content">
                <div id="featured_channel_name">
                    <b>
                        <a href={window.location.origin}>{props.name}</a>
                    </b>
                </div>
                <div id="featured_channel_avatar">
                    <a href={window.location.origin}>
                        <img id="featured_channel_image" src={props.avatar} />
                    </a>
                </div>
                <div id="featured_channel_data">
                    <div className="featured_channel_data_insert">
                        <span className="grayText">Videos: </span>{props.videos}
                    </div>
                    <div className="featured_channel_data_insert">
                        <span className="grayText">Subs: </span>{props.subs}
                    </div>
                    <div className="featured_channel_data_insert">
                        <span className="grayText">Style: </span>{props.style}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FeaturedChannel;
