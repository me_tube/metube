import './FeaturedChannels.css';
import FeaturedChannel from "./Channel/FeaturedChannel";

function FeaturedChannels() {
    return (
        <div id="featured_channels">
            <div id="featured_channels_content">
                <div id="featured_channels_header">Featured Channels</div>
                <FeaturedChannel avatar={window.location.origin + '/icons/profile_pic.png'} name="vjasieg" subs="2137420" videos="14" style="Lifestyle" />
                <FeaturedChannel avatar={window.location.origin + '/icons/profile_pic.png'} name="Kaczkomat" subs="12345" videos="14" style="Science" />
                <FeaturedChannel avatar={window.location.origin + '/icons/profile_pic.png'} name="bendard" subs="2137420" videos="14" style="Anime" />
                <FeaturedChannel avatar={window.location.origin + '/icons/profile_pic.png'} name="pi" subs="2137420" videos="14" style="Mathematics" />
                <div id="featured_channels_more">
                    <a href={window.location.origin}>See More Channels</a>
                </div>
            </div>
        </div>
    );
}

export default FeaturedChannels;
