import './MemberLogin.css';

function MemberLogin() {
    return (
        <div id="member_login">
            <div id="member_login_tab">
                <span id="member_login_text">Member Login</span>
            </div>
            <div id="member_login_table_div">
                <table>
                    <tbody>
                        <tr>
                            <td><b>User Name:</b></td>
                            <td><input tabIndex="1" type="text" name="username" value="" className="hpLoginField" /></td>
                        </tr>
                        <tr>
                            <td><b>Password:</b></td>
                            <td><input tabIndex="2" type="password" name="password" className="hpLoginField" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div id="member_login_signup">
                                    <b><a href={window.location.origin}>Sign Up</a></b>
                                </div>
                                <b>
                                    <div>
                                        <input id="member_login_btn" type="submit" value="Login" />
                                    </div>
                                    <div id="member_login_forgot">
                                        <b>Forgot: </b>
                                        <a href={window.location.origin}>Username</a> | <a href={window.location.origin}>Password</a>
                                    </div>
                                </b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default MemberLogin;
