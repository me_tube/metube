import './BlogNote.css';

function BlogNote() {
    return (
        <div id="blog_note">
            <div id="blog_note_content">
                <div id="blog_note_header">Read Our Blog!</div>
                <div id="blog_note_subheader"><b>On the 10-Minute limit!</b></div>
                Hey there 'Tubers!<br /><br />
                I want to touch base with you all about some questions that have come up recently regarding our Director accounts. Last fall we had a process that enabled Directors to upload ...
                <div id="blog_note_more">
                    <b>
                        <a href={window.location.origin}>Read more...</a>
                    </b>
                </div>
            </div>
        </div>
    );
}

export default BlogNote;
