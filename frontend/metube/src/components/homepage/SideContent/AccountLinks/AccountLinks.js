import './AccountLinks.css';

function AccountLinks() {
    return (
        <div id="hpAccountLinksDiv">
            <span className="account_links_main_text">My: </span>
            <a href={window.location.origin}  className="account_link">Videos</a> -&nbsp;
            <a href={window.location.origin}  className="account_link">Favorites</a> -&nbsp;
            <a href={window.location.origin}  className="account_link">Playlists</a> -&nbsp;
            <a href={window.location.origin}  className="account_link">Inbox</a> -&nbsp;
            <a href={window.location.origin} className="account_link">Subscriptions</a>
        </div>
    );
}

export default AccountLinks;
