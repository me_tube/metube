import './SideContent.css';
import AccountLinks from "./AccountLinks/AccountLinks";
import MemberLogin from "./MemberLogin/MemberLogin";
import SiteUpdates from "./SiteUpdates/SiteUpdates";
import BlogNote from "./BlogNote/BlogNote";
import FeaturedChannels from "./FeaturedChannels/FeaturedChannels";
import FeaturedCommunities from "./FeaturedCommunities/FeaturedCommunities";
import PopularMobileVideos from "./PopularMobileVideos/PopularMobileVideos";

function SideContent() {
    return (
        <div id="hpSideContent">
            <AccountLinks />
            <MemberLogin />
            <SiteUpdates />
            <BlogNote />
            <FeaturedChannels />
            <FeaturedCommunities />
            <PopularMobileVideos />
        </div>
    );
}

export default SideContent;
