import './SiteUpdates.css';


function SiteUpdates() {
    return (
        <div id="site_updates">
            <div id="site_updates_content">
                <div id="site_updates_title">New at MeTube</div>
                <div id="site_updates_details">
                    <div id="site_updates_image">
                        <a href={window.location.origin}>
                            <img id="testtube_img" src={window.location.origin + '/icons/testtube_icon.gif'}  />
                        </a>
                    </div>
                    <div id="site_updates_desc">
                        <b><a href={window.location.origin}>TestTube</a></b>
                        <br />
                        <span id="site_updates_desc_text">Try out new YouTube features in development, and let us know what you think!</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SiteUpdates;
