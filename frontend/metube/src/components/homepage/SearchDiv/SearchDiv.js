import './SearchDiv.css';

function SearchDiv() {
    return (
        <div id="searchDiv">
            <input tabIndex="10000" type="text" name="search_query" maxLength="128" id="searchField" />&nbsp;&nbsp;&nbsp;
            <input type="submit" name="search" value="Search" />
        </div>
    );
}

export default SearchDiv;
