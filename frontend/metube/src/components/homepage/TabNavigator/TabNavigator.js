import './TabNavigator.css';
function TabNavigator() {
    return (
        <div id="gNavDiv">
            <div id="upload">
                <p id="upload_text">Upload Videos</p>
                <img id="go_icon" src={window.location.origin + '/icons/go_icon.png'} alt="go_icon"/>
            </div>
            <div className="tab">
                <div className="tab_content">
                    <p className="tab_text">Community</p>
                </div>
            </div>
            <div className="tab">
                <div className="tab_content">
                    <p className="tab_text">Channels</p>
                </div>
            </div>
            <div className="tab">
                <div className="tab_content">
                    <p className="tab_text">Categories</p>
                </div>
            </div>
            <div className="tab">
                <div className="tab_content">
                    <p className="tab_text">Videos</p>
                </div>
            </div>
        </div>
    );
}

export default TabNavigator;
