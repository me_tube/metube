import './App.css';
import LogoTagDiv from "./components/homepage/LogoTag/LogoTagDiv";
import UtilDiv from "./components/homepage/UtilDiv/UtilDiv";
import SearchDiv from "./components/homepage/SearchDiv/SearchDiv";
import TabNavigator from "./components/homepage/TabNavigator/TabNavigator";
import BottomTab from "./components/homepage/TabNavigator/BottomTab";
import SideContent from "./components/homepage/SideContent/SideContent";

function App() {
  return (
    <div className="App">
      <div id="baseDiv">
        <LogoTagDiv />
        <UtilDiv />
        <SearchDiv />
        <TabNavigator />
        <BottomTab />
        <SideContent />
      </div>
    </div>
  );
}

export default App;
